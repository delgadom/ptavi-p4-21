#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys
import json
import time
# Constantes. Puerto.


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """
    registered = {}

    def json2registered(self):
        try:
            with open('registered.json', "r") as jsonfile:
                self.registered = json.load(jsonfile)
        except OSError as err:
            pass


    def registered2json(self):
        with open('registered.json', "w") as jsonfile:
            json.dump(self.registered, jsonfile, indent=4)


    def handle(self):
            """
            Handle SIP requests
            """

            self.json2registered()
            linea =  self.rfile.read().decode('utf-8')
            if (linea[0:8]) == 'REGISTER':
                    self.result = "200 OK"
                    self.expires = linea.split()[4]
                    self.actualtime = time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(int(time.time())+ int(self.expires)))
                    self.registered[linea.split()[1][4:]] = {
                                'client': self.client_address[0],
                                'expires': self.actualtime,
                                }

            if(linea.split()[4]) == '0':
                self.result = "200 OK"
                self.expires = linea.split()[4]
                del self.registered[linea.split()[1][4:]]

            (sender_ip, sender_port) = self.client_address
            self.wfile.write(f"SIP/2.0 {self.result}\r\n\r\n".encode())
            self.registered2json()


def main():
    # Listens at port PORT (my address)


    puerto =  int("".join(sys.argv[1]))
    try:
        serv = socketserver.UDPServer(('', puerto), SIPRegisterHandler)
        print(f"Server listening in port {puerto}")
    except OSError as e:
        sys.exit(f"Error empezando a escuchar: {e.args[1]}.")

    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
        sys.exit(0)


if __name__ == "__main__":
    main()
