#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys

# Constantes. Puerto.


class EchoHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """

    def handle(self):
        """
        handle method of the server class
        (all requests will be handled by this method)
        """

        for line in self.rfile:
            print (self.client_address[0], end =" ")
            print(self.client_address[1], end =" ")
            print(line.decode('utf-8'))
            self.wfile.write("Message: ".encode('utf-8')+line)


def main():
    # Listens at port PORT (my address)
    # and calls the EchoHandler class to manage the request

    puerto =  int("".join(sys.argv[1]))
    try:
        serv = socketserver.UDPServer(('', puerto), EchoHandler)
        print(f"Server listening in port {puerto}")
    except OSError as e:
        sys.exit(f"Error empezando a escuchar: {e.args[1]}.")

    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
        sys.exit(0)


if __name__ == "__main__":
    main()
