#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente UDP que abre un socket a un servidor
"""

import socket
import sys

# Constantes. Dirección IP del servidor y contenido a enviar
SERVER = 'localhost'
PORT = 6001
LINE = '¡Hola mundo!'



def main():

    mensaje = " ".join(sys.argv[3:])
    ip = "".join(sys.argv[1])
    puerto = int("".join(sys.argv[2]))


# Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
    try:

        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket.connect((ip, puerto))
            my_socket.send(mensaje.encode('utf-8'))
            data = my_socket.recv(1024)
            print( data.decode('utf-8'))
    except ConnectionRefusedError:
        print("Error conectando a servidor")


if __name__ == "__main__":
    main()
